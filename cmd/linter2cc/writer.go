package main

import (
	"bytes"
	"io"
)

// countWriter is used to detect if anything was written to a wrapped io.Writer
type countWriter struct {
	inner   io.Writer
	written int
}

func (w *countWriter) Write(p []byte) (int, error) {
	n, err := w.inner.Write(p)
	w.written += n
	return n, err
}

func (w *countWriter) Written() int {
	return w.written
}

// convertingWriter transforms the Code Climate output to ensure consistency and
// conformity (see Write()).
type convertingWriter struct {
	inner                    *countWriter
	removedOpeningArrBracket bool
}

func newConvertingWriter(w io.Writer) io.Writer {
	return &convertingWriter{
		inner:                    &countWriter{inner: w},
		removedOpeningArrBracket: false,
	}
}

// The Code Climate specification (https://github.com/codeclimate/platform/blob/master/spec/analyzers/SPEC.md#output)
// requires:
// - results to be separated by newlines or NUL bytes
// - results should be streamed (should not output a json array)
//
// Write() handles these cases and ensures consistency.
func (w *convertingWriter) Write(p []byte) (int, error) {
	add := 0

	p, add = w.removeArrayBrackets(p)
	p = w.convertEntrySeparator(p)

	n, err := w.inner.Write(p)

	return n + add, err
}

func (w *convertingWriter) removeArrayBrackets(p []byte) ([]byte, int) {
	add := 0

	// Remove opening '[' array indicator
	if w.inner.Written() == 0 && p[0] == '[' {
		add++
		p = p[1:]
		w.removedOpeningArrBracket = true
	}

	if w.removedOpeningArrBracket {
		// Remove closing ']' array indicator
		var trimmed bool
		p, trimmed = w.trimRight(p, ']')
		if trimmed {
			add++
		}
	}

	return p, add
}

func (w *convertingWriter) trimRight(p []byte, ch byte) ([]byte, bool) {
	length := len(p)
	if length > 0 && p[length-1] == ch {
		p = p[:length-1]
		return p, true
	}
	return p, false
}

// convertEntrySeparator converts any NUL bytes used to separate JSON entries into newlines for consistency
func (w *convertingWriter) convertEntrySeparator(p []byte) []byte {
	return bytes.ReplaceAll(p, []byte{0}, []byte{'\n'})
}
