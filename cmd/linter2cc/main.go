package main

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"gitlab.com/gitlab-org/language-tools/go/linters/linter2cc/linters"
	golangcilint "gitlab.com/gitlab-org/language-tools/go/linters/linter2cc/linters/golangci-lint"
	"gitlab.com/gitlab-org/language-tools/go/linters/linter2cc/linters/hadolint"
	"gitlab.com/gitlab-org/language-tools/go/linters/linter2cc/linters/markdownlint"
	"gitlab.com/gitlab-org/language-tools/go/linters/linter2cc/linters/shellcheck"
	"gitlab.com/gitlab-org/language-tools/go/linters/linter2cc/linters/yamllint"
)

var (
	// Populated during build
	version = "master"
	commit  = "?"
	date    = ""
)

const (
	ANSI_RED   = "\033[0;31m"
	ANSI_RESET = "\033[0;m"
)

func registerLinters() linters.Manager {
	manager := linters.New()
	linters := []linters.Linter{
		golangcilint.New(),
		shellcheck.New(),
		markdownlint.New(),
		hadolint.New(),
		yamllint.New(),
	}
	for _, linter := range linters {
		err := manager.Register(linter)
		if err != nil {
			fmt.Fprintln(os.Stderr, "invalid linter registration")
		}
	}
	return manager
}

func main() {
	manager := registerLinters()
	switch {
	case len(os.Args) < 2:
		printUsage(manager)
		os.Exit(1)
	case len(os.Args) == 2 && os.Args[1] == "--version":
		printVersion(manager)
		os.Exit(0)
	}

	linterPath := os.Args[1]
	linterName := filepath.Base(linterPath)
	linter := manager.Linter(linterName)
	if linter == nil {
		fmt.Fprintf(os.Stderr, ANSI_RED+"%q is not supported.\n", linterName)
		fmt.Fprintln(os.Stderr, "Please open a Merge Request at "+
			"https://gitlab.com/gitlab-org/language-tools/go/linters/linter2cc"+ANSI_RESET)
		os.Exit(1)
	}

	args := make([]string, len(os.Args)-2)
	copy(args, os.Args[2:])
	args = linter.CmdLineArgs(args)

	exitCode := runLinterCmd(linterPath, args, linter)
	os.Exit(exitCode)
}

func runLinterCmd(linterName string, args []string, linter linters.Linter) int {
	cmd := exec.Command(linterName, args...)

	linterOutReader, err := cmd.StdoutPipe()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Failed to connect to linter stdout")
		return 2
	}

	// Set up linter stderr stream. There are 2 scenarios:
	// 1. Linter outputs report to stdout and any errors to stderr (most usual scenario)
	// 2. Linter outputs report as well as errors to stderr (e.g. markdownlint)
	var linterStderr *countWriter
	switch linter.OutStream() {
	case os.Stderr:
		linterErrReader, err := cmd.StderrPipe()
		if err != nil {
			fmt.Fprintln(os.Stderr, "Failed to connect to linter stderr")
			return 2
		}

		linterOutReader = linterErrReader
	case os.Stdout:
		linterStderr = &countWriter{inner: os.Stderr}
		cmd.Stderr = linterStderr
	default:
		panic("invalid linter stream output")
	}

	err = cmd.Start()
	if err != nil {
		// Failed to start linter, dump the error to stderr
		fmt.Fprintln(os.Stderr, err)
		return 2
	}

	// Run linter
	errLint := linter.Run(newConvertingWriter(os.Stdout), linterOutReader)
	if errLint != nil {
		fmt.Fprintf(os.Stderr, "Error running linter conversion: %v\n", errLint)
		return 1
	}

	err = cmd.Wait()
	switch errWait := err.(type) {
	case *exec.ExitError:
		exitCode := errWait.ExitCode()
		if exitCode == 1 && (linterStderr == nil || linterStderr.written == 0) {
			// Linter should return 0 even if errors were discovered
			exitCode = 0
		}
		return exitCode
	default:
		if err != nil {
			return -1
		}
	}

	return 0
}

func printUsage(manager linters.Manager) {
	exe := filepath.Base(os.Args[0])

	linters := fmt.Sprintf("  %-20s\t%s\n", "Name", "URL")
	for _, l := range manager.Linters() {
		linters += fmt.Sprintf("- %-20s\t%s\n", l.Name(), l.URL())
	}

	fmt.Fprintf(os.Stderr, `%s has version %s built from %s on %s

Usage:

	%s --version:                   Show version information
	%s <linter> [linter arguments]: Run specified linter

The %d supported linters are:

%s
`,
		exe,
		version,
		commit,
		date,
		exe,
		exe,
		len(manager.Linters()),
		linters,
	)
}

func printVersion(manager linters.Manager) {
	exe := filepath.Base(os.Args[0])

	var linters []string
	for _, l := range manager.Linters() {
		linters = append(linters, l.Name())
	}

	fmt.Fprintf(os.Stderr, `%s has version %s built from %s on %s
  Supported linters: %s
`, exe, version, commit, date, strings.Join(linters, ", "))
}
