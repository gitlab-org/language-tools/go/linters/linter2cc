image: golang:1.14.2

variables:
  REPO_NAME: gitlab.com/gitlab-org/language-tools/go/linters/linter2cc

# The problem is that to be able to use go get, one needs to put
# the repository in the $GOPATH. So for example if your gitlab domain
# is gitlab.com, and that your repository is namespace/project, and
# the default GOPATH being /go, then you'd need to have your
# repository in /go/src/gitlab.com/namespace/project
# Thus, making a symbolic link corrects this.
before_script:
  - mkdir -p $GOPATH/src/$(dirname $REPO_NAME)
  - ln -svf $CI_PROJECT_DIR $GOPATH/src/$REPO_NAME
  - cd $GOPATH/src/$REPO_NAME

stages:
  - test
  - build
  - deploy

lint:
  stage: test
  image: golangci/golangci-lint:v1.27.0
  variables:
    REPORT_FILE: gl-code-quality-report.json
    LINT_FLAGS: "--color never --deadline 15m"
    OUT_FORMAT: code-climate
  script:
    - golangci-lint run ./... --out-format "${OUT_FORMAT}" ${LINT_FLAGS} | tee ${REPORT_FILE}
  timeout: 15 minutes
  artifacts:
    reports:
      codequality: ${REPORT_FILE}
    paths:
      - ${REPORT_FILE}
    when: always
    expire_in: 7d

compile:
  stage: build
  script:
    - go build -o linter2cc
      -ldflags "-s -w -X main.version=dev -X main.commit=$(git rev-parse --short HEAD)
      -X main.date=$(date -u '+%FT%TZ')" ./cmd/linter2cc
    - ./linter2cc --version
  artifacts:
    paths:
      - linter2cc

trigger_build:
  stage: deploy
  image: alpine:latest
  variables:
    GIT_STRATEGY: none
  before_script:
    - apk add --update curl
  script:
    - curl -X POST -F token=${RUNNER_LINTERS_TRIGGER_TOKEN} -F ref=master https://gitlab.com/api/v4/projects/19533665/trigger/pipeline
  only:
    refs:
      - master@gitlab-org/language-tools/go/linters/linter2cc
