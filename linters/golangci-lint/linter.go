package golangcilint

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strings"

	"gitlab.com/gitlab-org/language-tools/go/linters/linter2cc/codeclimate"
	"gitlab.com/gitlab-org/language-tools/go/linters/linter2cc/linters"
)

type linter struct {
}

func New() linters.Linter {
	return new(linter)
}

func (l *linter) Name() string {
	return "golangci-lint"
}

func (l *linter) URL() string {
	return "https://github.com/golangci/golangci-lint#install-golangci-lint"
}

func (l *linter) CmdLineArgs(args []string) []string {
	return append(args, "--out-format", "code-climate")
}

func (l *linter) OutStream() io.Reader {
	return os.Stdout
}

func (l *linter) Run(w io.Writer, r io.Reader) error {
	// The golangci-lint Code Climate output is a simple JSON array and therefore not compliant
	// with the Code Climate spec (https://github.com/codeclimate/platform/blob/master/spec/analyzers/SPEC.md#output),
	// so we need to re-encode the output as newline separated JSON objects
	enc := json.NewEncoder(w)
	dec := json.NewDecoder(r)

	// Convert to Code Climate entries
	var ccIssues []codeclimate.Issue
	err := dec.Decode(&ccIssues)
	if err == io.EOF {
		return nil
	}
	if err != nil {
		return fmt.Errorf("unmarshalling golangci-lint output: %w", err)
	}

	// Stream out Code Climate output
	for _, ccIssue := range ccIssues {
		// Output Code Climate JSON
		if ccIssue.Type == "" {
			ccIssue.Type = "issue"
		}
		if ccIssue.CheckName == "" {
			// Take the linter name from the `description` field and use that as `check_name`
			parts := strings.SplitN(ccIssue.Description, ": ", 3)
			switch len(parts) {
			case 2:
				ccIssue.CheckName = parts[0]
				ccIssue.Description = parts[1]
			case 3:
				ccIssue.CheckName = strings.Join(parts[0:2], ": ")
				ccIssue.Description = parts[2]
			}
		}
		err = enc.Encode(&ccIssue)
		if err != nil {
			return fmt.Errorf("marshalling Code Climate output: %w", err)
		}
	}

	return nil
}
