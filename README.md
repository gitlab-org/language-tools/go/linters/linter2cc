# linter2cc

Linter executor proxy whose goal is to provide output for supported linters in Code Climate format,
resorting to a conversion if necessary.

## Types of conversions performed

First of all, `linter2cc` adds any necessary command line arguments to the selected linter in order to have
a machine-readable format. In order of preference from available output formats:

- Native Code Climate output
- JSON output
- Line output (usually parsed with regular expressions)

### Native Code Climate output

Not all linters output the format of Code Climate reports mandated by
the [spec](https://github.com/codeclimate/platform/blob/master/spec/analyzers/SPEC.md#output).
Some, like `golangci-lint` or `shellcheck` output a full comma-separated JSON array:

```json
[
    { ... },
    { ... }
]
```

In this case `linter2cc` will remove the enclosing square brackets and replace the separator commas with newlines, as
per the spec.

### Output

Linters are inconsistent in the choice of stream for dumping out the report. While most use `stdout`,
`markdownlint` for example dumps out to `stderr`, so `linter2cc` adapts to this situation and ensures
that reports are dumped to `stdout` as per the Code Climate spec.

## Usage

### Example 1: shellcheck

```shell
# Instead of running the linter directly:
shellcheck -x *

# Prefix it with linter2cc, and the output will be done in Code Climate format
linter2cc shellcheck -x *
```

Input:

```json
[{"file":"/Users/pedropombeiro/src/gitlab.com/gitlab-org/gitlab-runner/scripts/go_test_with_coverage_report","line":40,"endLine":40,"column":32,"endColumn":38,"level":"info","code":2086,"message":"Double quote to prevent globbing and word splitting.","fix":{"replacements":[{"line":40,"endLine":40,"precedence":14,"insertionPoint":"afterEnd","column":32,"replacement":"\"","endColumn":32},{"line":40,"endLine":40,"precedence":14,"insertionPoint":"beforeStart","column":38,"replacement":"\"","endColumn":38}]}},{"file":"/Users/pedropombeiro/src/gitlab.com/gitlab-org/gitlab-runner/scripts/go_test_with_coverage_report","line":94,"endLine":94,"column":20,"endColumn":26,"level":"info","code":2086,"message":"Double quote to prevent globbing and word splitting.","fix":{"replacements":[{"line":94,"endLine":94,"precedence":17,"insertionPoint":"afterEnd","column":20,"replacement":"\"","endColumn":20},{"line":94,"endLine":94,"precedence":17,"insertionPoint":"beforeStart","column":26,"replacement":"\"","endColumn":26}]}}]
```

Output:
```json
{"type":"issue","check_name":"SC2086","description":"Double quote to prevent globbing and word splitting.","location":{"path":"/Users/pedropombeiro/src/gitlab.com/gitlab-org/gitlab-runner/scripts/go_test_with_coverage_report","lines":{"begin":40,"end":40}},"severity":"info"}
{"type":"issue","check_name":"SC2086","description":"Double quote to prevent globbing and word splitting.","location":{"path":"/Users/pedropombeiro/src/gitlab.com/gitlab-org/gitlab-runner/scripts/go_test_with_coverage_report","lines":{"begin":94,"end":94}},"severity":"info"}
```

### Example 2: golangci-lint

```shell
# Instead of running the linter directly:
golangci-lint ./...

# Prefix it with linter2cc, and the output will be done in Code Climate format
linter2cc golangci-lint ./...
```

Input:
```json
[{"description":"ineffassign: ineffectual assignment to `isCopy`","fingerprint":"E44E035849DF68EFE65E7FCAC52E7961","location":{"path":"cmd/linter2cc/writer.go","lines":{"begin":94}}},{"description":"whitespace: unnecessary leading newline","fingerprint":"F817AFD80E1E176C4E542D90DD958635","location":{"path":"cmd/linter2cc/writer.go","lines":{"begin":82}}}]
```

Output:
```json
{"description":"ineffassign: ineffectual assignment to `isCopy`","fingerprint":"E44E035849DF68EFE65E7FCAC52E7961","location":{"path":"cmd/linter2cc/writer.go","lines":{"begin":94}}}
{"description":"whitespace: unnecessary leading newline","fingerprint":"F817AFD80E1E176C4E542D90DD958635","location":{"path":"cmd/linter2cc/writer.go","lines":{"begin":82}}}
```